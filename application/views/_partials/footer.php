    <div class="footer4 spacer b-t">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 m-b-30">
                    <h5 class="m-b-20">Alamat</h5>
                    <p>Gedung Lecture Hall Lantai 1 UIN Sunan Gunung Djati Bandung</p>
                </div>
                <div class="col-lg-4 col-md-12 m-b-30">
                    <h5 class="m-b-20">Telepon</h5>
                    <br/>Kantor :+62 817 0612 127</p>
                </div>
                <div class="col-lg-4 col-md-12 m-b-30">
                    <h5 class="m-b-20">Email</h5>
                    <p>Kantor : <a href="#" class="link">puslitpen@uinsgd.ac.id</a></p>
                </div>
                <!-- <div class="col-lg-3 col-md-6">
                    <h5 class="m-b-20">Sosial Media</h5>
                    <div class="round-social light">
                        <a href="#" class="link"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="link"><i class="fa fa-twitter"></i></a>
                        <a href="https://instagram.com/puslitpen_uin_bandung/" class="link"><i class="fa fa-instagram"></i></a>
                    </div>
                </div> -->
            </div>
            <div class="f4-bottom-bar">
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex font-14">
                            <div class="m-t-10 m-b-10 copyright">All Rights Reserved by Puslitpen.</div>
                            <!-- <div class="links ml-auto m-t-10 m-b-10">
                                <a href="#" class="p-10 p-l-0">Terms of Use</a>
                                <a href="#" class="p-10">Legal Disclaimer</a>
                                <a href="#" class="p-10">Privacy Policy</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>