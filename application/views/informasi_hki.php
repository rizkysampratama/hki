<!DOCTYPE html>
<html lang="en">

<head>
     <!-- Head -->
    <?php $this->load->view('_partials/header.php') ?>
</head>

<body class="">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">HKI</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
       <!-- NAVIGASI -->
        <?php $this->load->view('_partials/nav.php') ?>
        <!-- ============================================================== -->
        <!-- Top header  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="banner-innerpage" style="background-image:url(assets/images/innerpage/banner-bg2.jpg)">
                    <div class="container">
                        <!-- Row  -->
                        <div class="row justify-content-center ">
                            <!-- Column -->
                            <div class="col-md-6 align-self-center text-center" data-aos="fade-down" data-aos-duration="1200">
                                <h1 class="title">Informasi HKI</h1>
                                <h6 class="subtitle op-8">Data Informasi Hak Kekayaan Intelektual Dosen / Peneliti di Lingkungan UIN Sunan Gunung Djati Bandung</h6>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Static Slider 10  -->
                <!-- ============================================================== -->
                <div class="spacer feature26">
                    <div class="container">
                        <div class="row wrap-feature-26">
                            <div class="col-md-7 align-self-center">
                                <div class="max-box">
                                    <h2 class="title m-t-20">Hak Kekayaan Intelektual </h2>
                                    <p class="m-t-30"><b>Hak Kekayaan Intelektual, disingkat “HKI” atau adalah padanan kata yang biasa digunakan untuk Intellectual Property Rights (IPR), yakni hak yang timbul bagi hasil olah pikir yang menghasikan suatu produk atau proses yang berguna untuk manusia pada intinya HKI adalah hak untuk menikmati secara ekonomis hasil dari suatu kreativitas intelektual. Objek yang diatur dalam HKI adalah karya-karya yang timbul atau lahir karena kemampuan intelektual manusia.</b></p>
                                    <p>Secara garis besar HKI dibagi dalam 2 (dua) bagian, yaitu:</p>
                                    <p><ol>
                                        <li>Hak Cipta (copyright)</li>
                                        <li>Hak kekayaan industri (industrial property rights)
                                            <ul style="list-style-type:square">
                                              <li>Paten (patent)</li>
                                              <li>Desain industri (industrial design)</li>
                                              <li>Merek (trademark)</li>
                                              <li>Penanggulangan praktek persaingan curang (repression of unfair competition)</li>
                                              <li>Desain tata letak sirkuit terpadu (layout design of integrated circuit)</li>
                                              <li>Rahasia dagang (trade secret)</li>
                                            </ul>
                                        </li>
                                    </ol></p>
                                    <!-- <a href="javascript:void(0)" class="linking">Learn More <i class="ti-arrow-right"></i></a> -->
                                </div>
                            </div>
                            <div class="col-md-5 col-md-5"> <img src="assets/images/science.png" class="img-responsive" /> </div>
                        </div>
	                    <div class="row wrap-feature-26">
		                    <div class="col-md-12 align-self-center">
			                    <div class="max-box">
				                    <h2 class="title m-t-20">Grafik Hak Kekayaan Intelektual </h2>
				                    <h4 class="text-center">Grafik Hak Kekayaan Intelektual Berdasarkan Fakultas dan Prodi</h4>
				                    <br>
				                    <?php
				                    foreach($data_fakultas as $data_f){
					                    $fakultas[] = $data_f->nm_fakultas;
					                    $judul_fak[] = (float) $data_f->total_judul;
				                    }
				                    foreach($data_prodi as $data_p){
					                    $prodi[] = $data_p->nm_prodi;
					                    $judul_prod[] = (float) $data_p->total_judul;
				                    }
//				                    echo (count($data_prodi));
				                    ?>
				                    <canvas id="canvasFakultas" width="auto" height="150"></canvas>

				                    <hr>
				                    <br>

				                    <canvas id="canvasProdi" width="100" height="90"></canvas>


			                    </div>
		                    </div>
	                    </div>
                    </div>
                </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Back to top -->
        <!-- ============================================================== -->
        <a class="bt-top btn btn-circle btn-lg btn-info" href="#top"><i class="ti-arrow-up"></i></a>
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <?php $this->load->view('_partials/footer.php') ?>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('_partials/js.php') ?>
</body>
<script type="text/javascript" src="<?php echo base_url().'assets/chartjs/Chart.min.js'?>"></script>
<script>
    var ctx = document.getElementById("canvasFakultas");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: <?php echo json_encode($fakultas); ?>,
            datasets: [{
                label: 'Jumlah HKI',
                data: <?php echo json_encode($judul_fak); ?>,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 127, 0, 0.5)',
                    'rgba(75, 192, 192, 0.5)',
                    'rgba(255,215,0, 0.5)',
                    'rgba(255, 159, 64, 0.5)',
	                'rgba(255, 0, 199, 0.5)',
	                'rgba(8, 0, 255, 0.5)',
	                'rgba(0, 226, 7, 0.5)'
                ],
                borderColor: [
                    'rgba(255,99,132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 127, 0, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(255,215,0, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 0, 199, 1)',
                    'rgba(8, 0, 255, 1)',
                    'rgba(0, 226, 7, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
	        title:{
                display: true,
                text: 'Grafik Jumlah Hak Kekayaan Intelektual Fakultas'
	        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>

<script>
    new Chart(document.getElementById("canvasProdi"), {
        type: 'doughnut',
        data: {
            labels: <?php echo json_encode($prodi); ?>,
            datasets: [{
                label: "Jumlah",
                backgroundColor: getRandomColor(),
                data: <?php echo json_encode($judul_prod); ?>
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Grafik Jumlah Hak Kekayaan Intelektual Program Studi'
            }
        }
    });

    // var dynamicColors = function() {
    //     var r = Math.floor(Math.random() * 255);
    //     var g = Math.floor(Math.random() * 255);
    //     var b = Math.floor(Math.random() * 255);
    //     return "rgb(" + r + "," + g + "," + b + ")";
    // }

    function getRandomColor() {
        var data = <?php echo count($data_prodi) ; ?>;
        var letters = '0123456789ABCDEF'.split('');
        var colors = [];
        for (var a = 0; a < data; a++){
            var color = '#';
	        for (var i = 0; i < 6; i++ ) {
	            color += letters[Math.floor(Math.random() * 16)];
	        }
	        colors.push(color);
        }
        return colors;
    }
</script>

</html>