<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 05/02/2019
 * Time: 12:08
 */

class Login extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

	function index(){
		$this->load->view('lapor');
	}

	function auth(){
		$username=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

		$cek_user=$this->User_model->auth_user($username,$password);

//		die(var_dump($cek_user));

		if($cek_user->num_rows() > 0){ //jika login sebagai dosen
			$data=$cek_user->row_array();
			$this->session->set_userdata('masuk',TRUE);
			if($data['level']=='1'){ //Akses admin
				$this->session->set_userdata('akses','1');
				$this->session->set_userdata('ses_id',$data['nip']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				redirect('page');

			}else{ //akses dosen
				$this->session->set_userdata('akses','2');
				$this->session->set_userdata('ses_id',$data['nip']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				redirect('page');
			}

		}else{ //jika login sebagai mahasiswa
			$cek_mahasiswa=$this->login_model->auth_mahasiswa($username,$password);
			if($cek_mahasiswa->num_rows() > 0){
				$data=$cek_mahasiswa->row_array();
				$this->session->set_userdata('masuk',TRUE);
				$this->session->set_userdata('akses','3');
				$this->session->set_userdata('ses_id',$data['nim']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				redirect('page');
			}else{  // jika username dan password tidak ditemukan atau salah
				$url=base_url();
				echo $this->session->set_flashdata('msg','Username Atau Password Salah');
				redirect($url);
			}
		}

	}
}